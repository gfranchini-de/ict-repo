.PHONY: uploads all

#TARGETS=authorized_keys employees-keyring.asc
TARGETS=authorized_keys

all: uploads

authorized_keys: $(wildcard keys/ssh/*)
	cat $? > $@

uploads: $(patsubst %, upload-%, $(TARGETS)) $(TARGETS)

upload-%: %
	curl -X POST --user "${BB_AUTH_STRING}" "https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@$<

